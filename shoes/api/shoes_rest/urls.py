from django.urls import path
from .views import bin_list, shoe_list, shoe_detail


urlpatterns = [
    path("bins/", bin_list, name="bin_list"),
    path("bins/<int:pk>/", bin_list, name="bin_list_detail"),
    path("shoes/", shoe_list, name="shoe_list"),
    path("shoes/<int:pk>/", shoe_detail, name="shoe_detail"),
]
