import React, {useEffect, useState} from 'react'

function HatForm() {

  const [fabric, setFabric] = useState('')
  const [style, setStyle] = useState('')
  const [color, setColor] = useState('')
  const [pictureUrl, setPictureUrl] = useState('')
  const [location, setLocation] = useState('')
  const [locations, setLocations] = useState([])

  const handleFabricChange = (event) => {
    const value = event.target.value
    setFabric(value)
  }
  const handleStyleChange = (event) => {
    const value = event.target.value
    setStyle(value)
  }
  const handleColorChange = (event) => {
    const value = event.target.value
    setColor(value)
  }
  const handlePictureUrlChange = (event) => {
    const value = event.target.value
    setPictureUrl(value)
  }
  const handleLocationChange = (event) => {
    const value = event.target.value
    setLocation(value)
  }

  const fetchData = async () => {
    const url = "http://localhost:8100/api/locations/"

    const response = await fetch(url)

    if (response.ok) {
        const data = await response.json()
        setLocations(data.locations)
    }
}


  const handleSubmit = async (event) => {
    event.preventDefault()

    const data = {}

    data.fabric = fabric
    data.style = style
    data.color = color
    data.picture_url = pictureUrl
    data.location = location

    const hatsUrl = 'http://localhost:8090/api/hats/'

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const response = await fetch(hatsUrl, fetchConfig)
    if (response.ok) {
      const newHat = await response.json()
      console.log(newHat)


      setFabric('')
      setStyle('')
      setColor('')
      setPictureUrl('')
      setLocation('')
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return(
    <div className="row">
      <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
              <h1>Add New Hat</h1>
              <form onSubmit={handleSubmit} id="add-hat-form">
                  <div className="form-floating mb-3">
                      <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                      <label htmlFor="fabric">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                      <input onChange={handleStyleChange} value={style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                      <label htmlFor="style_name">Style</label>
                  </div>
                  <div className="form-floating mb-3">
                      <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                      <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                      <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" />
                      <label htmlFor="picture_url">Photo</label>
                  </div>
                  <div className="mb-3">
                      <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                          <option value="">Choose a Location</option>
                          {locations.map(location => {
                              return (
                                  <option key={location.id} value={location.id}>
                                      {location.closet_name}
                                  </option>
                              )
                          })}
                      </select>
                  </div>
                  <button type="button" className="btn btn-primary">Add Hat</button>
              </form>
          </div>
      </div>
    </div>
  )
}
export default HatForm
