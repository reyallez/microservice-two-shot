from django.db import models
from django.urls import reverse

# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)

class Hat(models.Model):
    name = models.CharField(max_length=50)
    fabric = models.CharField(max_length=50)
    style = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name='hats',
        on_delete=models.CASCADE,
    )
    def get_api_url(self):
        return reverse("hat_detail", kwargs={"pk": self.pk})
