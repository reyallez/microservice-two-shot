from django.shortcuts import render
from .models import BinVO, Shoe
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
    ]


@require_http_methods(["GET", "POST"])
def bin_list(request):
    if request.method == "GET":
        bin = BinVO.objects.all()
        return JsonResponse(
            {"bins": bin},
            encoder=BinVOEncoder
        )
    else:
        content = json.loads(request.body)
        bin = BinVO.objects.create(**content)
        return JsonResponse(
            bin,
            encoder=BinVOEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def bin_details(request, pk):
    if request.method == "GET":
        try:
            bin = BinVO.objects.get(id=pk)
            return JsonResponse(
                bin,
                encoder=BinVOEncoder,
                safe=False
            )
        except BinVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            bin = BinVO.objects.get(id=pk)
            bin.delete()
            return JsonResponse(
                bin,
                encoder=BinVOEncoder,
                safe=False,
            )
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            bin = BinVO.objects.get(id=pk)

            props = ["closet_name", "bin_number", "bin_size"]
            for prop in props:
                if prop in content:
                    setattr(bin, prop, content[prop])
            bin.save()
            return JsonResponse(
                bin,
                encoder=BinVOEncoder,
                safe=False,
            )
        except BinVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]

    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def shoe_list(request):
    if request.method == "GET":
        shoe = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoe},
            encoder=ShoeEncoder
        )
    else:
        content = json.loads(request.body)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def shoe_detail(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)

            props = ["manufacturer", "model_name", "color", "picture_url","bin"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeEncoder,
                safe=False
            )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
