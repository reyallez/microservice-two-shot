# Wardrobify

Team:

* Rob Claus - Shoes
* Rey Xeka - Hats

## Design

## Shoes microservice

I am going to first set up my models, then I am going to set up a view. I want to set up polling and make sure it is receiving all of the data it needs from the wardrobe_api. Once I get those communicating and pulling the right data I can apply my other models and views. Test out that all of my get,post,put,delete methods are working properly. Then I will go through react once my back end is working.

## Hats microservice

I set up a LocationVO model to import location data from the wardrobe api as well as a single model for Hat with all the required parameters and linked it to LocationVO using a forgeignKey. Created views and encoders to display the Hat list and individual hat details and set up polling with the wardrobe api. Using the database I then created a react form to create new hat entries and a list display page to show all hats stored in the database along with a delete function for each entry.
