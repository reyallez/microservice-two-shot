import { React } from "react"
import { Link } from "react-router-dom"

function HatList(props) {
  const handleDelete = async (hat) => {
    fetch(`http://localhost:8090/api/hats/${hat}`, {
        method: "delete",
        headers: {
            "Content-Type": "application/json"
        }
    }).then(() => {
        window.location.reload()
    })
  }

  return (
    <div className="container">
          <table className="table table-shadow table-hover">
            <thead>
              <tr>
                <th>Fabric</th>
                <th>Style</th>
                <th>Color</th>
                <th>Photo</th>
              </tr>
            </thead>
            <tbody>
              {props.hats?.map(hat => {
                return (
                  <tr key={hat.href}>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style }</td>
                    <td>{ hat.color }</td>
                    <td><img src={hat.picture_url} alt="" width="30%" height="30%" /> </td>
                    <td><button onClick={() => handleDelete(hat.id)} type="button" className="btn btn-outline-danger">Delete</button></td>
                  </tr>
                );
              })}
            </tbody>
           </table>
           <div>
            <Link to="/hats/new">
              <button type="button" className="btn btn-outline-primary">Add Hat</button>
            </Link>
           </div>
        </div>
  )
}

export default HatList
