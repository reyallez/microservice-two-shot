import React, {useEffect} from 'react';

function ShoeList(props) {

    const handleDelete = async (href) => {
        fetch(`http://localhost:8080${href}`, {
            method: "delete",
            headers: {
                "Content-Type": "application/json"
            }
        }).then(() => {
            window.location.reload()
        })
      }


    return (
    <div className="col">
        {props.shoes?.map(shoe => {
            return (
            <div key={shoe.href} className="card mb-3 shadow">
                <img src={shoe.picture_url} className="card-img-top" />
                <div className="card-body">
                <h5 className="card-title">{shoe.manufacturer}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                    {shoe.model_name}
                </h6>
                <p className="card-text">
                    {shoe.color}
                </p>
                <div className="card-footer">
                    <button onClick={() => handleDelete(shoe.href)}> delete </button>
                </div>
                </div>
            </div>
            );
        })}
    </div>
    );
    }

    export default ShoeList;
