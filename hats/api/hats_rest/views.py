from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import LocationVO, Hat
from django.views.decorators.http import require_http_methods
import json


# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "id",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "fabric",
        "color",
        "id",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style",
        "fabric",
        "color",
        "id",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }


@require_http_methods(['GET', "POST"])
def hat_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_id = content["location"]
            location = LocationVO.objects.get(id=location_id)
            print(location)
            content["location"] = location
            hats = Hat.objects.create(**content)
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def hat_detail(request, pk):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist. Please try again."})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse(
            {"Shoe is deleted": count > 0}
        )
    else:
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)
            props = ["fabric", "style", "color", "picture_url", "location"]

            for prop in props:
                if prop in props:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist. Please try again."})
            response.status_code = 404
            return response
